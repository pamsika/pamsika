var longitude = null;
var latitude = null;
var deviceUUID = null;
var place_lat;
var place_long;
var place_name;
var isConnected = false;
var phoneGapLoaded = false;
var isSearching = true;
var count_total = 0;
var categoryName;
var notificationSet = false;
var count = 0;

document.addEventListener('deviceready', function(){
    phoneGapLoaded = true;
    navigator.geolocation.watchPosition(succ, fail, {timeout: 60000, enableHighAccuracy: true,  maximumAge:90000});
}, false);

function succ(position){
    longitude = position.coords.longitude;
    latitude = position.coords.latitude;
}

function fail(error){
    longitude = null;
    latitude = null;
}

(function()
{
 "use strict";
 /*
   hook up event handlers 
 */
    var commodity;
    var search_by = 'Cost';
    var district;
    var category;
    var district1;
    var back = false;
    var searchval;
    var holder;
 function register_event_handlers()
 {
    $(document).ajaxStart(function(){
        if(!isSearching){
           $(".fade").show();  
            $(".ajax_loader_div").show();    
        }
    }) .ajaxStop(function(){
        $(".fade").hide();
        $(".ajax_loader_div").hide();
    });
     
     $("#notification").click(function(){
        
         notification(category);
     });
     
    $("#search_item_options1 li a").click(function(){
            $("#search_by").text($(this).text());
            search_by = $(this).text();
    });
     
     $("#banks").click(function(){
         
         //getLocation();
         category = 2;
         $("#category").html("<h3><b style='color:#00B6F1'>Banks & ATMs</b></h3>");
         $("#selector_desc").html("<h4><b>View Banks or ATM machines from other districts by selecting a district<b></h4>");
          categoryName = "Bank";
         
         activate_page("#selector");
         
    });
    
     
     $("#restraurants").click(function(){
         activate_page("#selector");         
        $("#selector_desc").html("<h4><b>View Restaurants from other districts by selecting a district</b></h4>");
         $("#category").html("<h3><b style='color:#00B6F1'>Restaurants</b></h3>");
          categoryName = "Restaurant";
         category = 5;
    });
     
     $("#gasStations").click(function(){
         activate_page("#selector");
         $("#selector_desc").html("<h4><b>View Gas Stations from other districts by selecting a district</b></h4>");
         $("#category").html("<h3><b style='color:#00B6F1'>Gas Stations</b></h3>");   
         categoryName = "Gas Station";
         category = 6;
    });
     
     $("#hotels").click(function(){
         activate_page("#selector");
         $("#selector_desc").html("<h4><b>View Hotels from other districts by selecting a district</b></h4>");
         $("#category").html("<h3><b style='color:#00B6F1'>Hotels and Lodges</b></h3>");
         categoryName = "Hotel or Lodge";
         category = 7;
    });
     
     $("#bars").click(function(){
         activate_page("#selector");
         $("#selector_desc").html("<h4><b>View Bars from other districts by selecting a district</b></h4>");
         $("#category").html("<h3><b style='color:#00B6F1'>Bars</b></h3>");
          categoryName = "Bar";
         category = 8;
    });
    
     $("#district_specific_back").click(function(){
        activate_page("#searchResults");
     });
     
     $("#hospitals").click(function(){
         
         activate_page("#selector");
         
        $("#category").html("<h3><b style='color:#00B6F1'>Hospitals</b></h3>");
         $("#selector_desc").html("<h4><b>View Hospitals from other districts by selecting a district</b></h4>");
         categoryName = "Hospital";
         category = 3;
    });
     
     $("#shops").click(function(){
         activate_page("#selector");
         
         $("#category").html("<h3><b style='color:#00B6F1'>Shops</b></h3>");
         $("#selector_desc").html("<h4><b>View Shops from other districts by selecting a district</b></h3>");
         categoryName = "Shop";
         category = 1;
    });
         $("#police").click(function(){
        activate_page("#selector");     
         
             $("#category").html("<h3><b style='color:#00B6F1'>Police Stations</b></h3>");
             $("#selector_desc").html("<h4><b>View Police Stations from other districts by selecting a district</b></h4>");
             categoryName = "Police Station";
             category = 4;
         });
   
   
     // closest stuff
     
      $("#closest").click(function(){
          isSearching = false;
          back =  true;
          if(longitude != null && latitude != null){
              $.post("http://www.netsoftmw.com/paMsika.php", {longitude:longitude, latitude:latitude, category:category}, function(data){
                $("#filter_div").show();
                $("#dataResults").html(data);
                    if(data == "<h3 style='color:red;margin-left:20px;'><i><b>No places found</b><i></h3>"){
                        $("#filter_div").hide();  
                        $("#results_num_div").hide();
                    }
                  else{
                      $("#results_num_div").show();
                    count_total = $("#dataResults a").size();
                   $("#results_total").html("<h4 style='color:#B4009E'><i>Found " + count_total + " results</i></h4>");
                  }
                    activate_page("#searchResults");
             });             
          }
          else{
              alert("Failed to get GPS coordinates of your location. Please check that your GPS is enabled");
          }
    });
     
     $("#district1").click(function(){
         isSearching = false;
         back = true;
         district1 = $("#district1").val();
         if(longitude != null && latitude != null){
             $.post("http://www.netsoftmw.com/paMsika.php", {longitude:longitude, latitude:latitude, category:category, district1:district1}, function(data){
                $("#dataResults").html(data);
                $("#filter_div").show(); 
                if(data == "<h3 style='color:red;margin-left:20px;'><i><b>No commodity found</b><i></h3>"){
                    $("#filter_div").hide();   
                }
                    activate_page("#searchResults");
             });
         }
         else{
             alert("Failed to get GPS coordinates of your location. Please check that your GPS is enabled");
         }
    });
     
     
     
       $("#results_back").click(function(){
          if(back==true){
                activate_page("#selector");
          }
           else{
                activate_page("#mainpage");
           }
       });
     
     
     
       $("#like").click(function(){
            // $.post("http://www.netsoftmw.com/paMsika.php",{deviceUUID: deviceUUID, sh},function(data){
             //alert(searchval);
		   //$("#texts").html(data);   
	  //});
    });
     
    $("#searchBar").keyup(function(){
        isSearching = true;
	    searchval= $("#searchBar").val();
        if(longitude != null && latitude != null){
            $.post("http://www.netsoftmw.com/paMsika.php",{searchval:searchval},function(data){
                 //alert(searchval);
               $("#texts").html(data);   
           });
        }
        else{
            alert("Failed to get GPS coordinates of your location. Please check that your GPS is enabled");
        }
	}).focusout(function(){$("#texts").html(data);});
     
     
      $("#texts").click(function(){
          holder = $("#texts").text();
          $("#searchBar").val(holder);
          $("#texts").text("");
    });
    
    $("#filter_result_list").keyup(function(){
        var count = 0;
        var filter = $(this).val();
        $("#dataResults a").each(function(){
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).hide();
            } else {
                $(this).show();
                count++;
            }
        });
        var numberItems = count;
        
        if($.trim(filter) == ""){
            $("#results_total").html("<h4 style='color:#B4009E'><i>Found " + count_total + " results</i></h4>");
        }
        else{
            $("#results_total").html("<h4 style='color:#B4009E'><i>Showing " + numberItems + " of " + count_total + " results</i></h4>");
        }
    });
     $('#district_search_go').click(function(ev){
         isSearching = false;
         district1 = $("#district_list2").val();
        back = true;
         if(longitude != null && latitude != null){
             $.post("http://netsoftmw.com/paMsika.php", {longitude:longitude, latitude:latitude, category:category, district1:district1}, function(data){
                $("#filter_div").show(); 
                 $("#dataResults").html(data);
                    if(data == "<h3 style='color:red;margin-left:20px;'><i><b>No places found</b><i></h3>"){
                        $("#filter_div").hide(); 
                        $("#results_num_div").hide();
                    }
                 else{
                     $("#results_num_div").show();
                     count_total = $("#dataResults a").size();
                     $("#results_total").html("<h4 style='color:#B4009E'><i>Found " + count_total + " results</i></h4>");                   
                 }
                 activate_page("#searchResults");
            });             
         } 
         else{
             alert("Failed to get GPS coordinates of your location. Please check that your GPS is enabled");
         }
     });     
     
     $("#district_specific_home").click(function(){
        activate_page("#mainpage");
     });
    $("#search").click(function(){
        back = false;
        commodity = $("#searchBar").val();
        isSearching = false;
        district = $("#district").val();

        if(commodity != ""){
            if(search_by == 'Cost'){
                   $.post("http://www.netsoftmw.com/nodistrict.php",{commodity: commodity, search_by: search_by},function(data){
                        $("#filter_div").show();
                       $("#dataResults").html(data);
                        if(data == "<h3 style='color:red;margin-left:20px;'><i><b>No commodity found</b><i></h3>"){
                            $("#filter_div").hide(); 
                            $("#results_num_div").hide();
                        }
                         else{
                             $("#results_num_div").show();
                             count_total = $("#dataResults a").size();
                             $("#results_total").html("<h4 style='color:#B4009E'><i>Found " + count_total + " results</i></h4>");                   
                         }
                        activate_page("#searchResults");
                   });                         
           }
           else if(search_by == 'Location'){
                if(longitude != null && latitude != null){
                    $.post("http://www.netsoftmw.com/nodistrict.php",{commodity: commodity, search_by: search_by, district: district, longitude:longitude, latitude:latitude},function(data){
                        $("#filter_div").show(); 
                        $("#dataResults").html(data);
                        if(data == "<h3 style='color:red;margin-left:20px;'><i><b>No commodity found</b><i></h3>"){
                            $("#filter_div").hide(); 
                            $("#results_num_div").hide();
                        }
                         else{
                             $("#results_num_div").show();
                             count_total = $("#dataResults a").size();
                             $("#results_total").html("<h4 style='color:#B4009E'><i>Found " + count_total + " results</i></h4>");                   
                         }                        
                        activate_page("#searchResults");
                    });                      
                }
               else{
                   alert("Failed to get GPS coordinates of your location. Please check that your GPS is enabled");
               }
           }
            
            else if(search_by == 'Ratings'){
                 $.post("http://www.netsoftmw.com/nodistrict.php",{commodity: commodity, search_by: search_by, district: district, longitude:longitude, latitude:latitude},function(data){
                $("#filter_div").show(); 
                     $("#dataResults").html(data);
                    if(data == "<h3 style='color:red;margin-left:20px;'><i><b>No commodity found</b><i></h3>"){
                        $("#filter_div").hide(); 
                        $("#results_num_div").hide();
                    }
                     else{
                         $("#results_num_div").show();
                         count_total = $("#dataResults a").size();
                         $("#results_total").html("<h4 style='color:#B4009E'><i>Found " + count_total + " results</i></h4>");                   
                     }
                    activate_page("#searchResults");                     
                });               
           }
        }
    });

}

 document.addEventListener("app.Ready", register_event_handlers, false);
})();

function shopClick(itm_id){
    isSearching = false;
    if(longitude != null && latitude != null){
        $.post("http://www.netsoftmw.com/paMsika.php",{itm_id: itm_id},function(data){
        $("#description").html(data);
        place_lat = $('#place_lat').text();
        place_long = $("#place_long").text();
        place_name = $("#place_name").text();
        activate_page("#place_desc");
        initialize();
     });
    }
    else{
        alert("Failed to get GPS coordinates of your location. Please check that your GPS is enabled"); 
    }
}

function checkNetworkConnection(){
    
        isConnected = true;
    
}

function shopClickCategory(plc_id){
    isSearching = false;
    if(longitude != null && latitude != null){
          $.post("http://www.netsoftmw.com/paMsika.php",{plc_id: plc_id},function(data){
                $("#description").html(data);
                place_lat = $('#place_lat').text();
                place_long = $("#place_long").text();
                place_name = $("#place_name").text();
                activate_page("#place_desc");
                initialize();
         });       
    }
    else{
        alert("Failed to get GPS coordinates of your location. Please check that your GPS is enabled");        
    }   
}

function initialize() {    
var trafficLayer = new google.maps.TrafficLayer();
   trafficLayer.setMap(null);        
   ///document.getElementById("map").innerHTML = "";
    
  var latlngbounds = new google.maps.LatLngBounds();
  var mapProp = {
    center:new google.maps.LatLng(latitude,longitude),
       mapTypeId:google.maps.MapTypeId.ROADMAP,
      
 
  };
  var map=new google.maps.Map(document.getElementById("map"),mapProp);
    var markers = [
        ['current location', latitude, longitude],
        [place_name, place_lat,place_long]
    ]; 
    
    var infowindow = new google.maps.InfoWindow(), marker, i;
    for (i = 0; i < markers.length; i++) {
        var data = markers[i];
        var icon = "";
        if(i == 0){
            icon = 'green';                   
        }
        if(i == 1){
            icon = 'red';   
        }
        icon = "http://maps.google.com/mapfiles/ms/icons/" + icon + ".png";
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(markers[i][1], markers[i][2]),
            map: map,
            icon:icon
        });
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infowindow.setContent(markers[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));
        latlngbounds.extend(marker.position);
    }
    
    var bounds = new google.maps.LatLngBounds();
    map.setCenter(latlngbounds.getCenter());
    map.fitBounds(latlngbounds);
}

function likeShop(plc_id, itm_id){
     isSearching = false;
    deviceUUID = device.uuid;
    if(deviceUUID != null){
        $.post("http://www.netsoftmw.com/paMsika.php", {place_id_like: plc_id, itm_id_like:itm_id, device_uuid: deviceUUID}, function(data){
           alert(data);    
        });       
    }
    else{
        alert("Failed to get your device UUID. Please restart the application");
    }
}

function likePlace(plc_id){
    isSearching = false;
    deviceUUID = device.uuid;
    if(deviceUUID != null){
         $.post("http://www.netsoftmw.com/paMsika.php", {place_id_like: plc_id, device_uuid: deviceUUID, place_like: '1'}, function(data){
             alert(data);
        });         
    }
    else{
        alert("Failed to get your device UUID. Please restart the application");
    }  
}






function notification(cat){
    
    if(notificationSet == false){
        
            notificationSet = true;
        
            navigator.notification.alert("You will receive a notification when passing by a "+categoryName);
            navigator.notification.beep(1);
            navigator.notification.vibrate(500);
            getNotification(cat);
            
                 
    }
    else{
        notificationSet = false;
        navigator.notification.alert("You have unset the notifacation. This means you will not receive any");
    }
  
}
function getNotification(cat){ 
    if(notificationSet && count<600){
        //navigator.notification.alert("bha");
        $.post("http://netsoftmw.com/notification.php", {longitude:longitude, latitude:latitude, cat:cat}, function(data){
            if(data == 1){
                navigator.notification.beep(3);
                navigator.notification.vibrate(2000);
                navigator.notification.alert("You are passing by a "+categoryName);
                notificationSet = false;
            }
             count++;
         });  
        setTimeout(getNotification, 30000);
    }
    
       
}

